<h1>Töölepanek</h1>
<p>Kui backend server töötab, siis kaustas ./frontend kasutada käsku:</p>

```
npm install
npm start
```

<strong>Tööst</strong>
---
1. Seal võib esineda vigu.
1. Privaatses/incognito režiimis Cookie'id ei salvestu, mis teeb sisselogimise võimatuks.
1. Kindlasti tuleks kasutada aadressi http://localhost:3000/ või https://localhost:3000/, kuna need on ainsad aadressid, mis on CORS'is lubatud. Vastasel juhul ei toimu kasutaja ja serveri vahel ühendust.
1. Sama nimega kaustu saab luua, see ei tekita probleemi, aga kui laadia mitu sama nimega faili üles, siis ainult üks neist laetakse. Isegi siis kui laed erinevasse kausta.
1. Kuna kasustatud mahtu arvutatakse megabaitides (ümardamine toimub kuni kümnendikeni), siis see ei pruugi täpne olla.
1. Kui avada kaust ja siis luua uus kaust või laadida fail üles, siis luuakse fail sinna kausta mis viimati avati. See tähendab et kui sulgeda lahtiolev kaust ja siis luua/üleslaadida uus kaust/fail, siis luuakse kaust/fail juurkausta.
