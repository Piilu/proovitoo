﻿namespace filesystemAPI.Models
{
    public class GetUploadFiles
    {
        public string id { get; set; }
        public string parentId { get; set; }
        public string name { get; set; }
        public string children { get; set; }
        public Boolean isFile { get; set; }
        public string size { get; set; }
        public string created { get; set; }
        public string url { get; set; }
    }
}
