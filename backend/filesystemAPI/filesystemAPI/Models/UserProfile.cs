﻿namespace filesystemAPI.Models
{
    public class UserProfile
    {
        public string fullname { get; set; }   
        public string Email { get; set; }
        public string Nimi { get; set; }
        public string Parool { get; set; }
    }
}
