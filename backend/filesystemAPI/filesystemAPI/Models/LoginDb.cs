﻿namespace filesystemAPI.Models
{
    public class LoginDb
    {
        public string Email { get; set; }
        public string Nimi { get; set; }
        public string Parool { get; set; }
        public Double Ruumi { get; set; }
    }
}
