﻿namespace filesystemAPI.Models
{
    public class UploadFiles
    {
        public string parentId { get; set; }
        public string name { get; set; }
        public string children { get; set; }
        public Boolean isFile { get; set; }
        public string size { get; set; }
        public string url { get; set; }
    }
}
