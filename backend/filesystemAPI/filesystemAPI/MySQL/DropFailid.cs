﻿using Microsoft.AspNetCore.Mvc;

//Kustutab 'Failid' tabeli
namespace filesystemAPI.MySQL
{
    [Route("api/[controller]")]
    [ApiController]
    public class DropFailid : ControllerBase
    {
        public IConfiguration Configuration { get; }
        public DropFailid(IConfiguration Config)
        {
            Configuration = Config;
        }
        // GET: api/<DropFailid>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            var connectionsString = Configuration.GetConnectionString(name: "DefaultConnection");

            var results = new List<LoginDb>();
            MySqlConnection conn = new MySqlConnection(connectionsString);
            try
            {
                conn.Open();

                string sql = "DROP TABLE Failid";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.ExecuteNonQuery();
                Console.WriteLine("Failid Deleted");
                conn.Close();

                return new string[] { "Failid Deleted" };


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return new string[] { "Error" };

            }
        }

    }
}
