﻿using Microsoft.AspNetCore.Mvc;

 
//Loob 'Kasutaja' tabeli 
namespace filesystemAPI.MySQL
{
    [Route("api/[controller]")]
    [ApiController]
    public class MakeKasutajad : ControllerBase
    {

        public IConfiguration Configuration { get; }
        public MakeKasutajad(IConfiguration Config)
        {
            Configuration = Config;
        }
        // GET: api/<MakeKasutajad>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            var connectionsString = Configuration.GetConnectionString(name: "DefaultConnection");


            var results = new List<LoginDb>();
            MySqlConnection conn = new MySqlConnection(connectionsString);
            try
            {
                conn.Open();

                string sql = "CREATE TABLE Kasutajad (Fullname VARCHAR(100), Email VARCHAR(100), Nimi VARCHAR(50), Parool VARCHAR(50), Ruumi FLOAT, PRIMARY KEY (Email) )";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.ExecuteNonQuery();
                Console.WriteLine("Kasutajad Created");
                conn.Close();

                return new string[] { "Kasutajad Created" };


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return new string[] { "Error" };

            }
        }
    }


        
    
}
