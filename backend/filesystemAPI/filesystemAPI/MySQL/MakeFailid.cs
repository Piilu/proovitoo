﻿using Microsoft.AspNetCore.Mvc;

//Loob 'Failid' tabeli
namespace filesystemAPI.MySQL
{
    [Route("api/[controller]")]
    [ApiController]
    public class MakeFailid : ControllerBase
    {
        public IConfiguration Configuration { get; }
        public MakeFailid(IConfiguration Config)
        {
            Configuration = Config;
        }
        // GET: api/<MakeFailid>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            Configuration.GetConnectionString(name: "DefaultConnection");
            var connectionsString = Configuration.GetConnectionString(name: "DefaultConnection");

            var results = new List<LoginDb>();
            MySqlConnection conn = new MySqlConnection(connectionsString);
            try
            {
                conn.Open();

                string sql = "CREATE TABLE Failid (id INT NOT NULL AUTO_INCREMENT, parentId VARCHAR(255), name VARCHAR(50), children VARCHAR(255), isFile BOOLEAN, size VARCHAR(100), URL VARCHAR(255), Created VARCHAR(255),  Email VARCHAR(255), PRIMARY KEY (id) )";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.ExecuteNonQuery();
                Console.WriteLine("Created Failid");
                conn.Close();
                return new string[] { "Created Failid" };


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return new string[] { "Error" };

            }

        }


    }
}
