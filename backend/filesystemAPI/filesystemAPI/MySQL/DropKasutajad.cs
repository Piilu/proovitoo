﻿using Microsoft.AspNetCore.Mvc;

//Kustutab 'Kasutajad' tabeli
namespace filesystemAPI.MySQL
{
    [Route("api/[controller]")]
    [ApiController]
    public class DropKasutajad : ControllerBase
    {
        public IConfiguration Configuration { get; }
        public DropKasutajad(IConfiguration Config)
        {
            Configuration = Config;
        }
        // GET: api/<DropKasutajad>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            var connectionsString = Configuration.GetConnectionString(name: "DefaultConnection");


            var results = new List<LoginDb>();
            MySqlConnection conn = new MySqlConnection(connectionsString);
            try
            {
                conn.Open();

                string sql = "DROP TABLE Kasutajad";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.ExecuteNonQuery();
                Console.WriteLine("Kasutajad Deleted");
                conn.Close();

                return new string[] { "Kasutajad Deleted" };


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return new string[] { "Error" };

            }
        }
    }

    
}
