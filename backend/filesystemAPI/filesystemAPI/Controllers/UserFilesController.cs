﻿using Microsoft.AspNetCore.Mvc;

//Võtab ja lisab faile andmebaasist
namespace filesystemAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserFilesController : ControllerBase
    {
        public IConfiguration Configuration { get; }
        public UserFilesController(IConfiguration Config)
        {
            Configuration = Config;
        }
        // GET: api/<UserFilesController>
        [HttpGet]
        public IEnumerable<object> Get()
        {
            var email = HttpContext.Session.GetString("KEY");

            var connectionsString = Configuration.GetConnectionString(name: "DefaultConnection");

            var results = new List<GetUploadFiles>();
            MySqlConnection conn = new MySqlConnection(connectionsString);
            try
            {
                conn.Open();

                string sql = "SELECT * From Failid WHERE Email="+"'"+email+"'"+"";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                var dataid = rdr.GetOrdinal("id");
                var dataparentid = rdr.GetOrdinal("parentId");
                var dataname = rdr.GetOrdinal("name");
                var datachildren = rdr.GetOrdinal("children");
                var dataisFile = rdr.GetOrdinal("isFile");
                var datasize = rdr.GetOrdinal("size");
                var datacreated = rdr.GetOrdinal("Created");
                var dataurl = rdr.GetOrdinal("url");

                while (rdr.Read())
                {
                    results.Add(new GetUploadFiles()
                    {
                        id = rdr.GetInt32(dataid).ToString(),
                        parentId = rdr.GetString(dataparentid),
                        name = rdr.GetString(dataname),
                        children = null,
                        isFile = rdr.GetBoolean(dataisFile),
                        size = rdr.GetString(datasize),
                        created = rdr.GetString(datacreated),
                        url = rdr.GetString(dataurl),
                    }); ;
                }

                rdr.Close();
                conn.Close();

                return results;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return new string[] { "Error" };
            }
            Console.WriteLine("Done.");
        }



        // POST api/<UserFilesController>
        [HttpPost]
        public UploadFiles Post(UploadFiles request)
        {
            var email = HttpContext.Session.GetString("KEY"); HttpContext.Session.GetString("KEY");

            var connectionsString = Configuration.GetConnectionString(name: "DefaultConnection");

            var results = new List<UserProfile>();
            MySqlConnection conn = new MySqlConnection(connectionsString);
            try
            {
                conn.Open();
                Console.WriteLine(request.isFile);
                string sql = "INSERT INTO Failid (parentId,name,children,isFile,size,URL, Created, Email) VALUES (" + "'" + request.parentId + "'" + "," + "'" + request.name + "'" + "," + "'" + request.children + "'" + "," +  request.isFile + "," + "'" + request.size + "'" +","+"'"+request.url+"'"+"," + "'" + DateTime.Today.ToShortDateString() + "'" + ","+"'" +email+"'"+ ")";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                //return "error";
            }
            conn.Close();

            return request;

            Console.WriteLine("uploaded");


        }

    }
}
