﻿using Microsoft.AspNetCore.Mvc;

//Eemaldab kasutaja Cookie, ehk logib kasutaja välja
namespace filesystemAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LogoutController : ControllerBase
    {
        // GET: api/<LogoutController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            HttpContext.Session.Clear();
            return new string[] { HttpContext.Session.GetString("KEY") };
        }
    }
}
