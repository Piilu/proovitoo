﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
//Vastutab selle eest, et kasutaja millega sisselogitakse ikka eksisteerib ja parool on õige
//Sellels töös ei ole parool krupteeritud, aga võiks
namespace filesystemAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginsController : ControllerBase
    {
        public IConfiguration Configuration { get; }
        public LoginsController(IConfiguration Config)
        {
            Configuration = Config;
        }
        // GET: api/<ValuesController>
        [HttpGet]
        public  IEnumerable<string> Get()
        {
           return new string[] { HttpContext.Session.GetString("KEY") };
        }
        // POST api/<ValuesController>
        [HttpPost]
        public string[] Post(Login response)
        {


            var connectionsString = Configuration.GetConnectionString(name: "DefaultConnection");

            var results = new List<LoginDb>();
            MySqlConnection conn = new MySqlConnection(connectionsString);
            try
            {
                conn.Open();

                string sql = "SELECT * From Kasutajad WHERE Email = " + "'" + response.email + "'" + "";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                var dataemail = rdr.GetOrdinal("Email");
                var dataname = rdr.GetOrdinal("Nimi");
                var datapassword = rdr.GetOrdinal("Parool");
                var dataspace = rdr.GetOrdinal("Ruumi");

                while (rdr.Read())
                {
                    results.Add(new LoginDb
                    {
                        Email = rdr.GetString(dataemail),
                        Nimi = rdr.GetString(dataname),
                        Parool = rdr.GetString(datapassword),
                        Ruumi = rdr.GetDouble(dataspace),
                    });
                }

                rdr.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            conn.Close();
            Console.WriteLine("Done.");
            if (results.Count() != 0)
            {
                if (results[0].Parool == response.password)
                {

                    HttpContext.Session.SetString("KEY", results[0].Email);

                    return new string[] { "suc" };

                }
                else
                {
                    return new string[] { "Vale parool"};
                }
            }
            else
            {
                return new string[] { "Kasutajat pole olemas" };
            }
        }

    }
}

