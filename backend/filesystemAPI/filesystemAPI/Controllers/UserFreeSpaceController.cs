﻿using Microsoft.AspNetCore.Mvc;

//Hoiab silma peal kasutaja andmbekasutusel
namespace filesystemAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserFreeSpaceController : ControllerBase
    {
        public IConfiguration Configuration { get; }
        public UserFreeSpaceController(IConfiguration Config)
        {
            Configuration = Config;
        }
        // GET: api/<UserFreeSpaceController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            var email = HttpContext.Session.GetString("KEY");
            //var connectionsString = "server=proovi-sql.masin.one;port=13314;database=ProoviDb;user=ProoviDb;password=XCkPrGCO5m0J05P5hrBKr;";

            var connectionsString = Configuration.GetConnectionString(name: "DefaultConnection");

            var results = new List<UserFiles>();
            MySqlConnection conn = new MySqlConnection(connectionsString);
            try
            {
                conn.Open();

                string sql = "SELECT * From Kasutajad WHERE Email = " + "'" +email + "'" + "";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                var dataspace = rdr.GetOrdinal("Ruumi");

                while (rdr.Read())
                {
                    results.Add(new UserFiles
                    {
                        Ruumi = rdr.GetDouble(dataspace),
                    });
                }

                rdr.Close();
                conn.Close();
                return new string[] { results[0].Ruumi.ToString() };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return new string[] { "Error" };
            }
            
            Console.WriteLine("Done.");
            return new string[] { results[0].Ruumi.ToString()};
        }

        // POST api/<UserFreeSpaceController>
        [HttpPost]
        public string[] Post(UserFiles request)
        {
            var email = HttpContext.Session.GetString("KEY"); HttpContext.Session.GetString("KEY");

            var connectionsString = Configuration.GetConnectionString(name: "DefaultConnection");

            var results = new List<UserFiles>();
            MySqlConnection conn = new MySqlConnection(connectionsString);
            try
            {
                conn.Open();

                string sql = "SELECT * From Kasutajad WHERE Email = " + "'" + email + "'" + "";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                var dataspace = rdr.GetOrdinal("Ruumi");

                while (rdr.Read())
                {
                    results.Add(new UserFiles
                    {
                        Ruumi = rdr.GetDouble(dataspace),
                    });
                }

                rdr.Close();
                conn.Close();
                var freespace = (results[0].Ruumi-request.Ruumi);
                if (freespace >= 0)
                {

                    try
                    {
                        conn.Open();

                        sql = "UPDATE Kasutajad SET Ruumi =" + "'" + freespace.ToString() + "'" + " WHERE Email=" + "'" + email + "'" + "";
                        cmd = new MySqlCommand(sql, conn);
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                    conn.Close();

                }
                return new string[] { freespace.ToString() };

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            

            return null;


        }

    }
}
