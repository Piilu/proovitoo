﻿using Microsoft.AspNetCore.Mvc;

//Lisab uue kasutaja andmed andmebaasi
//Sellels töös ei ole parool krupteeritud, aga võiks
namespace filesystemAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegisterController : ControllerBase
    {
        public IConfiguration Configuration { get; }
        public RegisterController(IConfiguration Config)
        {
            Configuration = Config;
        }

        // POST api/<RegisterController>
        [HttpPost]
        public new string[] Post(Register request)
        {

            var connectionsString = Configuration.GetConnectionString(name: "DefaultConnection");

            MySqlConnection conn = new MySqlConnection(connectionsString);
            try
            {
                conn.Open();

                string sql = "SELECT * From Kasutajad  WHERE Email = " + "'" + request.email + "'" + "";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                if (rdr.HasRows)
                {
                    conn.Close();


                    return new string[] { "Kasutaja juba olemas" };
                }
                else
                {
                    conn = new MySqlConnection(connectionsString);
                    try
                    {
                        conn.Open();

                        sql = "INSERT INTO Kasutajad (Fullname,Email,Nimi,Parool,Ruumi) VALUES (" +"'"+request.fullname+"'"+","+"'" + request.email + "'" + "," + "'" + request.username + "'" + "," + "'" + request.password + "'" + "," + "'" + 500 + "'" + ")";
                        cmd = new MySqlCommand(sql, conn);
                        cmd.ExecuteNonQuery();
                        conn.Close();
                        HttpContext.Session.SetString("KEY", request.email);

                        return new string[] { "suc" };

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                        conn.Close();

                        return new string[] { "Midagi läks viltu"};
                    }

                 
                }
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                conn.Close();

                return new string[] { "Midagi läks viltu" };

            }
        }


    }
}
