﻿using Microsoft.AspNetCore.Mvc;

//Saadab kasutajale tema andmed ning muudab neid 
namespace filesystemAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UpdateProfileController : ControllerBase
    {
        public IConfiguration Configuration { get; }
        public UpdateProfileController(IConfiguration Config)
        {
            Configuration = Config;
        }
        // GET: api/<UpdateProfileController>
        [HttpGet]
        public IEnumerable<object> Get()
        {
            var email = HttpContext.Session.GetString("KEY");

            var connectionsString = Configuration.GetConnectionString(name: "DefaultConnection");

            var results = new List<UserProfile>();
            MySqlConnection conn = new MySqlConnection(connectionsString);
            try
            {
                conn.Open();

                string sql = "SELECT * From Kasutajad WHERE Email = " + "'" + email + "'" + "";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                var dataemail = rdr.GetOrdinal("Email");
                var dataname = rdr.GetOrdinal("Nimi");
                var datafullname = rdr.GetOrdinal("Fullname");
                var datapassword = rdr.GetOrdinal("Parool");

                while (rdr.Read())
                {
                    results.Add(new UserProfile
                    {
                        Email = rdr.GetString(dataemail),
                        Nimi = rdr.GetString(dataname),
                        fullname = rdr.GetString(datafullname),
                        Parool = rdr.GetString(datapassword),
                    });
                }

                rdr.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return null;
            }

            conn.Close();
            Console.WriteLine("Done.");
            return results;
        }




        // POST api/<UpdateProfileController>
        [HttpPost]
        public string[] Post(UserProfile response)
        {

            var connectionsString = Configuration.GetConnectionString(name: "DefaultConnection");

            var results = new List<UserProfile>();
            MySqlConnection conn = new MySqlConnection(connectionsString);
            try
            {
                conn.Open();

                string sql = "UPDATE Kasutajad SET Fullname = "+"'"+response.fullname+"'"+","+" Nimi =" + "'" + response.Nimi + "'" + " , Parool=" + "'" + response.Parool + "'" + " WHERE Email="+"'" +response.Email+"'"+"";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.ExecuteNonQuery();

                return new string[] { "updated" };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return new string[] { "error" };
            }
                conn.Close();
                Console.WriteLine("Updated");

        }

    }
}
