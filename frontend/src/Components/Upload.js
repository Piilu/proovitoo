/*Vastutab selle eest et üles laetav fail jõuab MINIO serveri õigesse kausta
Upload toimub läbi frontendi. Backend saadab ainult lingi mis annab ligipääsu Serverile.
Link on aktiivne 5 minutit
*/
import React, { useRef, useState } from 'react';
import { Form, Button, ProgressBar } from 'react-bootstrap';
import S3 from 'react-aws-s3';
import axios from "axios";
axios.defaults.withCredentials = true;

function Upload(props) {
    const fileInput = useRef();
    const { selectedFolder, setNewTree, newTree, setFreeSpace, freeSpace } = props
    const [progress, setProgress] = useState(0);
    const [displayProgress, setDisplayProgress] = useState("none");
    const [disableUpload, setDisableUpload] = useState(false);
    const handleUpload = async e => {
        e.preventDefault()
        if (fileInput.current.files[0]) {

            var fileLocation = selectedFolder
            let fileParent = selectedFolder ? selectedFolder.id : "0";
            let file = fileInput.current.files[0];
            const bytesToMegaBytes = bytes => bytes / (1024 ** 2);
            if (parseInt(freeSpace)-bytesToMegaBytes(file.size) <= 0) {
                alert("Ei ole piisavalt ruumi")
            }
            else {

                axios.post("https://localhost:7138/api/UploadFilesS3/get-url", { filename: file.name }).then(res => {

                    if (res.status === 200) {
                        var url = res.data;
                        var formData = new FormData();
                        formData.append("file", file);
                        const xhr = new XMLHttpRequest();
                        xhr.open("PUT", url);
                        xhr.upload.addEventListener("progress", e => {
                            setDisplayProgress("")
                            setDisableUpload(true)
                            const progress = e.lengthComputable ? (e.loaded / e.total) * 100 : 0;
                            setProgress(progress)
                            if (progress == 100) {
                                setDisplayProgress("none")
                                setDisableUpload(false)
                                setProgress(0);
                                const imageurl = url.split("?")[0]
                                const sizeRounded = Number(bytesToMegaBytes(file.size).toFixed(1))

                                axios.post("https://localhost:7138/api/UserFiles", selectedFolder ? { parentId: fileParent, name: file.name, children: "", isFile: true, size: sizeRounded.toString(), url: imageurl } : { parentId: "0", name: file.name, children: "", isFile: true, size: sizeRounded.toString(), url: imageurl }).then(res => {
                                    if (res.status === 200) {
                                        const newItems = []
                                        newTree.forEach((value) => {
                                            newItems.push(value)

                                        })
                                        newItems.push(res.data)
                                        setNewTree(newItems)
                                        axios.post("https://localhost:7138/api/UserFreeSpace", { Ruumi: sizeRounded  }).then(space => {
                                            setFreeSpace(space.data)
                                        });


                                    }
                                    else {
                                        console.log(res.data)
                                    }
                                })
                                fileInput.current.value = "";
                            }
                        });
                        xhr.setRequestHeader("Content-Type", "multipart/form-data")
                        xhr.send(file);
                    }
                    else {
                        alert("Error")
                    }

                })
            }
        }

        else {
            alert("Faili(le) pole valitud")
        }

    }

    return (
        <Form onSubmit={handleUpload} controlId="formFileMultiple" className="mb-3">
            <Form.Control type="file" ref={fileInput} />
            <Form.Control disabled={disableUpload} type="submit" value="Lae üles" />

            <ProgressBar style={{ display: displayProgress, width: "100%" }} variant="success" animated now={progress} />
        </Form>
    );
}

export default Upload;

