//Navigatsiooniriba
import axios from 'axios';
import React from 'react';
import { Container, Nav, Navbar } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import "../Style/navbar.css"
function MyNav(props) {
  const {activeHome, activeProfile, setAuth} = props
  const handleLogout=()=>{
    axios.get("https://localhost:7138/api/Logout").then(res=>{
      setAuth(res.data[0])
    })
  }
  return (
<Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
  <Container>
  <Navbar.Brand style={{fontSize:"1.7em"}} href="/">Pilv</Navbar.Brand>
  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
  <Navbar.Collapse id="responsive-navbar-nav">
    <Nav className="me-auto">

    </Nav>
    <Nav>
      <Link onClick={handleLogout} className={"navitem "} to="/">Logi välja</Link>
      <Link className={"navitem "+activeHome} to="/">Home</Link>
      <Link className={"navitem "+activeProfile} to="/profile">Profile</Link>
    </Nav>
  </Navbar.Collapse>
  </Container>
</Navbar>

  );
}

export default MyNav;