//Milline näeb välja sisselogimisleht
//Ja info liiklus serveri kus tehakse Session(Cookie), mis on aktiivne 60 päeva
import React, { useEffect, useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import "../Style/auth.css"
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';
axios.defaults.withCredentials = true;
function Login(props) {
    const {setAuth} =props;
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const navigate = useNavigate()
    const handleSubmit = () => {
        axios.post("https://localhost:7138/api/Logins",{email,password}).then(res => {
            if (res.data[0] !== "suc") {
                alert(res.data);
            }
            else if(!res){
                alert("Midagi läks viltu");
            }
            else {
                setAuth(res.data);
            }
        })
    }
    const handleEnter = e => {
        if(e.keyCode ==13){
            handleSubmit()
        }
    }

    return (
        <div className='center-form'>
            <div className="center">
                <h2>Logi sisse</h2>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Email aadress</Form.Label>
                    <Form.Control onKeyDown={handleEnter} value={email} onChange={(e) => setEmail(e.target.value)} type="email" placeholder="Sisesta email" />

                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Parool</Form.Label>
                    <Form.Control onKeyDown={handleEnter} value={password} onChange={(e) => setPassword(e.target.value)} type="password" placeholder="Parool" />
                </Form.Group>
                <Button onClick={handleSubmit} variant="primary" type="submit">
                    Logi sisse
                </Button>
 
                <Form.Group>

                    <p>Sul pole kasutajat? <Link to="/register">Registeeri kohe</Link></p>
                </Form.Group>
            </div>
        </div >
    );
}

export default Login;