// Annab võimaluse uuele failile nime panna
import React, { useEffect, useState } from 'react';
import { Form, Button, Alert,Modal } from 'react-bootstrap';

function MyModal(props) {
    const{selectedFolder,show,handleClose,handleNewFolder,folderName,setFolderName} = props

    return (
        <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Uus kaust</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <p>Uus kaust luuakse {selectedFolder?"'"+selectedFolder.name+"'"+" kausta":"juurkausta"} </p>
          <Form onSubmit={(e)=>{e.preventDefault()}}>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Control
                type="text"
                value={folderName}
                placeholder="Sisesta kausta nimi"
                onChange={(e)=>{setFolderName(e.target.value)}}
                autoFocus
                onKeyDown={(e)=>{if(e.keyCode===13){handleNewFolder(folderName)}
                }}
              />
            </Form.Group>
           
          </Form>
        </Modal.Body>
        <Modal.Footer>

          <Button variant="primary" onClick={()=>{handleNewFolder(folderName)}}>
            Loo kaust
          </Button>
        </Modal.Footer>
      </Modal>
    );
}

export default MyModal;