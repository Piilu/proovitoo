//Milline näeb uus fail välja
import React, { useEffect, useState } from 'react';
import { AiOutlineDownload} from 'react-icons/ai';

function FileItem(props) {
    const {name, size, modified,icon,item,selectItem} =props

    return (
        <div  className={"item-file " }>
            <div className='file'><span className='icon'>{icon}</span>{name}</div>
            <div className="size">{size}MB</div>
            <div className='modified'>{modified}</div>
           <div className='download-icon'> <a href={item.url}><AiOutlineDownload/></a></div>

        </div>
    );
}

export default FileItem;