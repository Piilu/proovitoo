//Milline näeb registreerimisleh välja
//Kantakse uus kasutaja andmebaasi ning tehakse Session(Cookie), mis on aktiivne 60 päeva
import React, { useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import "../Style/auth.css"
import { Link, useNavigate } from 'react-router-dom';
import axios from "axios";
function Register(props) {
    const { auth, setAuth } = props;
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [username, setUsername] = useState("");
    const [fullNimi, setFullNimi] = useState("")
    const navigate = useNavigate()
    const handleSubmit = () => {
        if (username.length >= 3 && password.length >= 6 && email && fullNimi) {
            const unique = email.split("@")[0] + email.split("@")[1]
            axios.post("https://localhost:7138/api/Register", { fullname: fullNimi, username, email, password }).then(res => {
                if (res.data[0] !== "suc") {
                    alert(res.data);
                }
                else if (!res) {
                    alert("Midagi läks viltu");
                }
                else {
                    navigate("/");
                    setAuth(res.data);
                }
            })
        } else {
            alert("Sisestatud andmed pole sobivad")
        }
    }
    const handleEnter = e => {
        if(e.keyCode ==13){
            handleSubmit()
        }
}
    return (
        <div className='center-form'>
            <div className="center">
                <h2>Registeeri</h2>
                <Form.Group className="mb-3" controlId="formBasicEesPerenimi">
                    <Form.Label>Ees- ja perenimi</Form.Label>
                    <Form.Control onKeyDown={handleEnter} onChange={(e) => { setFullNimi(e.target.value) }} type="text" value={fullNimi} placeholder="John Howard" />

                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicUsername">
                    <Form.Label>Hüüdnimi</Form.Label>
                    <Form.Control onKeyDown={handleEnter} value={username} onChange={(e) => setUsername(e.target.value)} type="text" placeholder="Hüüdnimi" />

                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Email aadress</Form.Label>
                    <Form.Control onKeyDown={handleEnter} value={email} onChange={(e) => setEmail(e.target.value)} type="email" placeholder="Sisesta email" />

                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Parool</Form.Label>
                    <Form.Control onKeyDown={handleEnter} value={password} onChange={(e) => setPassword(e.target.value)} type="password" placeholder="Sisesta parool" />
                </Form.Group>
                <Button onClick={handleSubmit} variant="primary" type="submit">
                    Registeeri
                </Button>
                <Form.Group>

                    <p>Sul pole kasutajat? <Link to="/">Logi sisse</Link></p>
                </Form.Group>
            </div>
        </div >
    );
}


export default Register;