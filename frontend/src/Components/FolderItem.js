//Milline näeb uus kaust välja
//Laetakse faile/kaustu mis asuvad kausta sees
import React, { useState } from 'react';
import {AiOutlineFileImage,AiOutlineFolder, AiOutlineFolderAdd, AiOutlineFile, AiFillFileZip,AiOutlineFilePdf,AiOutlineFileText,AiOutlineFileGif } from 'react-icons/ai';
import { ImFileVideo,ImFileMusic } from 'react-icons/im';

import FileItem from './FileItem';



function FolderItem(props) {
    const { name, subitems, item, selectItem } = props
    const [open, setOpen] = useState("closed")
    const [select, setSelect] = useState("")
    const handleClick = (e) => {
        selectItem(item)
        const select = e.currentTarget.id;
        if (select.includes("closed")) {
            setOpen("")
            setSelect("selected")
        }
        else {
            selectItem("")
            setSelect("")
            setOpen("closed")
        }

    }

    return (
        <div className={"item-folder "} >
            <div id={open} onClick={handleClick} className={'item-file folder ' + select}>

                <div className='file'><span className='icon'><AiOutlineFolder /></span>{name}</div>
            </div>
            <div className={'sub_files ' + open}>
                {subitems.length ? subitems.map((value) => {
                   var fileicon = <AiOutlineFile />
                   if (value.name.includes(".") && value.name.split(".")[1] === ("jpg") || value.name.split(".")[1] === ("png")) {
                       fileicon = <AiOutlineFileImage />
                   }
                   else if (value.name.includes(".") && value.name.split(".")[1] === ("zip")) {
                       fileicon = <AiFillFileZip />
                   }
                   else if(value.name.includes(".") && value.name.split(".")[1] === ("pdf")){
                       fileicon = <AiOutlineFilePdf/>
                   }
                   else if(value.name.includes(".") && value.name.split(".")[1] === ("mp4")){
                       fileicon = <ImFileVideo/>
                   }
                   else if(value.name.includes(".") && value.name.split(".")[1] === ("mp3")){
                       fileicon =<ImFileMusic/>
                   }
                   else if(value.name.includes(".") && value.name.split(".")[1] === ("txt")){
                       fileicon = <AiOutlineFileText/>
                       
                   }
                   else if(value.name.includes(".") && value.name.split(".")[1] === ("gif")){
                       fileicon = <AiOutlineFileGif/>
                   }
                    if (!value.isFile) {

                        return (

                            <FolderItem key={value.id} selectItem={selectItem} subitems={value.children} name={value.name} item={value} />
                        )
                    }
                    else{
                        return(

                            <FileItem modified={value.created}  icon={fileicon} selectItem={selectItem} key={value.id} item={value}  name={value.name} size={value.size}/>
                            )

                    }
                }) : "Tühi kaust"}
            </div>

        </div>
    );
}

export default FolderItem;