//Haldab mida näeb sisselogitud ja väljalogitud kasutaja
import './App.css';
import { Route, Routes, useLocation, useNavigate } from 'react-router-dom';
import Login from './Components/Login';
import Register from './Components/Register';
import Home from './Pages/Home'
import 'bootstrap/dist/css/bootstrap.min.css';
import Profile from './Pages/Profile';
import MyNav from './Components/MyNav';
import { useEffect, useState } from 'react';
import axios from 'axios';
axios.defaults.withCredentials = true;

function App() {
  const [auth,setAuth] =useState()
  const location = useLocation();
  const navigate = useNavigate("");
  useEffect(()=>{
    axios.get("https://localhost:7138/api/Logins").then(res=>{
      setAuth(res.data[0]);
    })
  },[location,auth])
  if(auth){
    return(
      <Routes>
       
      <Route path='/' element={< Home auth={auth} setAuth={setAuth} />}/>
      <Route path='/profile' element={<Profile auth={auth} setAuth={setAuth} />}/>
    </Routes>
    )
  }
  else{

    return (
        <Routes>
         
          <Route path='/' element={<Login  setAuth={setAuth}/>}/>
          <Route path='/register' element={<Register setAuth={setAuth} />}/>
        </Routes>
    );
  }
}

export default App;
