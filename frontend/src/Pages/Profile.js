//Kasutaja profiil, kus saab muuta kõiki andmeid peale emaili
import React, { useEffect, useState } from 'react';
import MyNav from '../Components/MyNav';
import "../Style/profile.css"
import { Form, Button, Alert } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
axios.defaults.withCredentials = true;


function Profile(props) {
    const { auth, setAuth } = props;
    const navigate = useNavigate("/");
    const [email, setEmail] = useState("");
    const [nimi, setNimi] = useState("");
    const [parool, setParool] = useState("")
    const [inputParool, setInputParool] = useState('')
    const [inputNimi, setInputNimi] = useState('');
    const [inputFullNimi, setInputFullNimi] =useState("");
    const [fullNimi,setFullNimi] = useState("")
    const [disabled, setDisabled] = useState(true)
    useEffect(() => {
        if (auth) {
            axios.get("https://localhost:7138/api/UpdateProfile",).then(res => {
                if(res.data){
                    setEmail(res.data[0].email)
                    setParool(res.data[0].parool)
                    setNimi(res.data[0].nimi)
                    setInputNimi(res.data[0].nimi)
                    setInputParool(res.data[0].parool);
                    setFullNimi(res.data[0].fullname);
                    setInputFullNimi(res.data[0].fullname);
                }
                else{
                    alert("Midagi läks viltu")
                }
            })
        }
    }, [])
    const handleUpdate = () => {
        axios.post("https://localhost:7138/api/UpdateProfile", {fullname:inputFullNimi, Email:email, Nimi: inputNimi, Parool: inputParool }).then(res => {
            if (res.data[0] === "updated") {
               
                window.location.reload("/");
            }
            else {
                alert(res.data[0])
            }
        })
    }

    const handleNameChange = (e) => {
        setInputNimi(e.target.value)
        if (e.target.value === nimi || e.target.value.length <= 3) {
            setDisabled(true)
        }
        else {
            setDisabled(false)
        }

    }
    const handleFullNameChange = (e) => {
        setInputFullNimi(e.target.value)
        if(e.target.value === fullNimi ||!e.target.value.length){
            setDisabled(true)
        }
        else{
            setDisabled(false)
        }

    }
    const handlePasswordChange = (e) => {
        setInputParool(e.target.value)
        if (e.target.value === parool || e.target.value.length <= 6) {
            setDisabled(true)

        }
        else {
            setDisabled(false)
        }

    }
    if (auth) {
        return (
            <div className='profile-page'>

                <MyNav setAuth={setAuth} auth={auth} activeProfile="active" route="/profile"></MyNav>

                <div className="content-container">
                    <div className="content-user">
                        <div className='profile-pic'>
                            <img src="https://thebenclark.files.wordpress.com/2014/03/facebook-default-no-profile-pic.jpg" alt='No PIC' />
                        </div>
                        <h2>{fullNimi}</h2>
                        <p>{email}</p>
                    </div>
                    <div className="content-data">
                        <h2>Minu andmed</h2>

                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Email aadress</Form.Label>
                            <Form.Control value={email} readOnly type="email" placeholder="Sisesta email" />

                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicEesPerenimi">
                            <Form.Label>Ees- ja perenimi</Form.Label>
                            <Form.Control onChange={handleFullNameChange} type="text" value={inputFullNimi} placeholder="John Howard" />

                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicHüüdnimi">
                            <Form.Label>Hüüdnimi</Form.Label>
                            <Form.Control onChange={handleNameChange} type="text" value={inputNimi} placeholder="John" />

                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicPassword">
                            <Form.Label>Parool</Form.Label>
                            <Form.Control onChange={handlePasswordChange} value={inputParool} type="password" placeholder="********" />

                        </Form.Group>
                        <Form.Group style={{ textAlign: "center" }} className='me-auto'>
                            <Button onClick={handleUpdate} disabled={disabled} variant="primary" type="submit">
                                Salvesta profiil
                            </Button>
                        </Form.Group>
                    </div>
                </div>

            </div >
        );
    }
    else {
        navigate("/")
    }
}

export default Profile;