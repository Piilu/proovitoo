//Failide haldamise leht
//Siin pannakse kõik vajalikud komponendid omale kohale ning neile antakse ka väärtus

import React, { useEffect, useState } from 'react';
import { Button, ProgressBar } from 'react-bootstrap';
import FileItem from '../Components/FileItem';
import MyNav from '../Components/MyNav';
import "../Style/filepage.css"
import { AiOutlineFileImage, AiOutlineFolderAdd, AiOutlineFile, AiFillFileZip,AiOutlineFilePdf,AiOutlineFileText,AiOutlineFileGif } from 'react-icons/ai';
import { ImFileVideo,ImFileMusic } from 'react-icons/im';

import FolderItem from '../Components/FolderItem';
import axios from 'axios';

import Upload from '../Components/Upload';
import { useNavigate } from 'react-router-dom';
import MyModal from '../Components/MyModal';

function Home(props) {
    const { auth, setAuth } = props
    const now = 60
    const available = 100 - now
    const [selectedFolder, setSelectedFolder] = useState()
    const navigate = useNavigate();
    const [freeSpace, setFreeSpace] = useState("");
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const [folderName, setFolderName] = useState("");
    const [newTree, setNewTree] = useState();
    const [lowSpace, setLowSpace] = useState("")
    useEffect(() => {
        axios.get("https://localhost:7138/api/UserFiles").then(res => {
            if (res.data[0] !== "Error") {

                setNewTree(res.data)
            }
            else {
                alert(res.data)
            }
        })

        axios.get("https://localhost:7138/api/UserFreeSpace").then(res => {
            if (res.data[0] !== "Error") {
                const spacceRounded = Number(parseFloat(res.data[0]).toFixed(1))
                setFreeSpace(spacceRounded)
                if (spacceRounded <= 20) {
                    setLowSpace("danger")
                }
                else {
                    setLowSpace("")

                }
            }
        })
    }, [freeSpace])
    const handleNewFolder = (folderName) => {
        const newItems = []

        newTree.forEach((value) => {
            newItems.push(value)


        })
        if (!folderName) {
            folderName = "Uus Kaust"
        }
        const newfolder =
        {
            // id: newItems.length ? (parseInt(newItems[newItems.length - 1].id)+1).toString():"1",
            name: folderName,
            parentId: selectedFolder ? selectedFolder.id : "0",
            isFile: false,
            size: "",
            children: null,
        }
        newItems.push(newfolder)
        folderUpload(newfolder);
        handleClose();
    }
    
    const selectItem = (item) => {
        setSelectedFolder(item)
    }
    const createNewFolder = () => {
        if (selectedFolder && selectedFolder.isFile) {
            alert("Valitud üksus ei ole kaust")
        }
        else {

            setFolderName("")
            handleShow()
        }

    }
    const folderUpload = (file) => {

        if (file) {

            axios.post("https://localhost:7138/api/UserFiles", { parentId: file.parentId, name: file.name, children: "", isFile: file.isFile, size: file.size, url: "" }).then(res => {
                if (res.status === 200) {
                    axios.get("https://localhost:7138/api/UserFiles").then(res => {
                        if (res.data[0] !== "Error") {

                            
                            setNewTree(res.data)
                        }
                        else {
                            alert(res.data)
                        }
                    })
                   
                }
                else {
                    console.log(res.data)

                }
            })
        }

    }
    if (auth) {
        return (
            <div className='file-page'>

                <MyModal selectedFolder={selectedFolder} setFolderName={setFolderName} folderName={folderName} handleNewFolder={handleNewFolder} show={show} handleClose={handleClose} />
                <MyNav auth={auth} setAuth={setAuth} activeHome="active" route="/profile"></MyNav>
                <div className="file-container">
                    <div className='file-browser-progress'>
                        <div className='my-progress'>
                            <ProgressBar variant={lowSpace} now={100 - (100 * parseInt(freeSpace) / 500)} label={freeSpace + "MB/500MB"} />

                        </div>
                    </div>
                    <div className='toolbar'>
                        <div>
                            <Button onClick={createNewFolder} style={{ fontSize: "1.5em" }} variant="light"><AiOutlineFolderAdd /></Button>


                        </div>
                        <div>
                            <Upload freeSpace={freeSpace} setFreeSpace={setFreeSpace} newTree={newTree} setNewTree={setNewTree} selectedFolder={selectedFolder} />

                        </div>
                        <div>

                        </div>
                    </div>
                    <div className='file-browser-nav' >
                        <div className='file'><p>Nimi</p></div>
                        <div className='size'><p>Maht</p></div>
                        <div className='modified'><p>Loodud</p></div>
                    </div>
                    <div className='file-view'>
                        {newTree ? newTree.length ? list_to_tree(newTree).map((value) => {
                            var fileicon = <AiOutlineFile />
                            if (value.name.includes(".") && value.name.split(".")[1] === ("jpg") || value.name.split(".")[1] === ("png")) {
                                fileicon = <AiOutlineFileImage />
                            }
                            else if (value.name.includes(".") && value.name.split(".")[1] === ("zip")) {
                                fileicon = <AiFillFileZip />
                            }
                            else if(value.name.includes(".") && value.name.split(".")[1] === ("pdf")){
                                fileicon = <AiOutlineFilePdf/>
                            }
                            else if(value.name.includes(".") && value.name.split(".")[1] === ("mp4")){
                                fileicon = <ImFileVideo/>
                            }
                            else if(value.name.includes(".") && value.name.split(".")[1] === ("mp3")){
                                fileicon =<ImFileMusic/>
                            }
                            else if(value.name.includes(".") && value.name.split(".")[1] === ("txt")){
                                fileicon = <AiOutlineFileText/>
                                
                            }
                            else if(value.name.includes(".") && value.name.split(".")[1] === ("gif")){
                                fileicon = <AiOutlineFileGif/>
                            }
                            if (!value.isFile) {

                                return (

                                    <FolderItem key={value.id} selectItem={selectItem} subitems={value.children} name={value.name} item={value} />
                                )
                            }
                            else {
                                return (

                                    <FileItem modified={value.created} icon={fileicon} selectItem={selectItem} key={value.id} item={value} name={value.name} size={value.size} />
                                )
                            }
                        }) : "Faile pole " : "Faile pole"}
                    </div>

                </div>
            </div>
        );
    }
    else {
        navigate("/")
    }
}

export default Home;

//Siin voetakse andmebaasist saadud list ja tehakse loetavaks failipuuks
function list_to_tree(list) {
    var map = {}, node, roots = [], i;

    for (i = 0; i < list.length; i += 1) {
        map[list[i].id] = i; 
        list[i].children = []; 
    }

    for (i = 0; i < list.length; i += 1) {
        node = list[i];
        if (node.parentId !== "0") {

            list[map[node.parentId]].children.push(node);
        } else {
            roots.push(node);
        }
    }
    return roots;
}
